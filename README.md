The files contained in this project carry out calculations for the 
quantification of dyes on paper substrates and accompanies the paper 
"Colorimetric Absorbance Mapping and Quantitation on Paper-Based Analytical 
Devices".

Required input for computation include:
-Image to be quantified (see To import a picture and split into R,G and B 
    channels)
-Correct threshold parameters after observation of the image histogram to 
    seporate dyed and non-dyed areas
-Dye specific fitting parameters from a set of standards 
    (see Conversion of absorbance values into quantity)
